package cucumber.shelf;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;

public class StepDefinitions {
    private Book _book = null;
    private Customer _customer = null;
    private Library _library = new Library();

    @Given("a customer named {string}")
    public void clientHasName(String name) {
        _customer = new Customer(name);
    }

    @Given("the book {string} is {string} the library")
    public void bookInLibrary(String bookName, String status) {
        _book = new Book(bookName);
        if (status.equals("in"))
            _library.addBook(_book);
    }

    @When("the customer tries to borrow the book")
    public void borrowAttempt() {
        _customer.borrow(_library, _book);
    }

    @Then("the customer has the book")
    public void customerHasBook() {
        assert _customer.hasBook(_book);
    }

    @Then("the customer doesn't have the book")
    public void customerDoesntHaveBook() {
        assert !_customer.hasBook(_book);
    }

    @Then("the book is listed as borrowed")
    public void isBorrowed() {
        assert _library.isBorrowed(_book);
    }
}
