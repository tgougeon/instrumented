Feature: Library

    Scenario: A new customer wants to borrow the first book he sees.
        Given a customer named "Dave"
        And the book "Waylander" is "in" the library
        When the customer tries to borrow the book
        Then the customer has the book
        And the book is listed as borrowed

    Scenario Outline: A customer can borrow books only if they are available.
        Given a customer named "<name>"
        And the book "<book_name>" is "<status>" the library
        When the customer tries to borrow the book
        Then the customer <may_have> the book

        Examples:
            |name |book_name           |status|may_have    |
            |Alice|Mistborne Vol.1      |in    |has         |
            |Bob  |Au Bonheur des Ogres|not in|doesn't have|
            |Bob  |Mistborn Vol.1      |not in|doesn't have|
            |Alice|Au Bonheur des Ogres|in    |has         |
            |Bob  |Au Bonheur des Ogres|in    |has         |
