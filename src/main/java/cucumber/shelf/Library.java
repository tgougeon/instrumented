package cucumber.shelf;

import java.util.ArrayList;
import java.util.HashMap;

public class Library {
    private HashMap<Book, Integer> _shelves;
    private HashMap<Customer, ArrayList<Book>> _borrows;

    public Library() {
        _shelves = new HashMap<>();
        _borrows = new HashMap<>();
    }

    public void addBook(Book book) {
        if (_shelves.containsKey(book))
            _shelves.replace(book, _shelves.get(book) + 1);
        else
            _shelves.put(book, 1);
    }

    void removeBook(Book book) {
        switch (_shelves.get(book)) {
            case 1:
                _shelves.remove(book);
                break;
            default:
                _shelves.replace(book, _shelves.get(book) - 1);
                break;
        }
    }

    public boolean isBorrowed(Book book) {
        for (ArrayList<Book> ab : _borrows.values()) {
            for (Book b : ab) {
                if (book.equals(b))
                    return true;
            }
        }
        return false;
    }

    public boolean borrow(Customer cstm, Book book) {
        if (!_shelves.containsKey(book))
            return false;
        if (_borrows.containsKey(cstm))
            _borrows.get(cstm).add(book);
        else {
            ArrayList<Book> cstmBooks = new ArrayList<>();
            cstmBooks.add(book);
            _borrows.put(cstm, cstmBooks);
        }
        removeBook(book);
        return true;
    }
}
