package cucumber.shelf;

import java.util.HashMap;

public class Customer {
    private String _name;
    private HashMap<Book, Integer> _books;

    public Customer(String name) {
        _name = name;
        _books = new HashMap<>();
    }

    public void borrow(Library lib, Book book) {
        int posessed = _books.containsKey(book) ? _books.get(book) : 0;
        if (lib.borrow(this, book))
            _books.put(book, posessed + 1);
    }

    public boolean hasBook(Book book) {
        return _books.containsKey(book);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Customer))
            return false;
        return ((Customer) other)._name.equals(_name);
    }
}
