package cucumber.shelf;

public class Book {
    private String _name;

    public Book(String name) {
        _name = name;
    }

    @Override
    public boolean equals(Object other) {
        if (other.getClass() != this.getClass())
            return false;
        return ((Book) other)._name.equals(_name);
    }

    @Override
    public int hashCode() {
        return _name.hashCode();
    }
}
