# Instrumented tests

This is a simple example program used for the [cucumber] presentation. It emulates the tests of a small library that allows to borrow books, but not to return them. It also doesn't have a `main` function, but that's OK since we're only here to showcase some instrumented tests.

## Run it on your machine

If you have `maven` and Java 17 on you machine, you too can run these tests at home ! You'll just need to run these commands to read the results :

```shell
git clone https://gitlab.emi.u-bordeaux.fr/tgougeon/instrumented
cd instrumented
mvn verify
open target/reports.html
```

Feel free to tinker with the code as much as you want, it's also meant to be a [cucumber] playground if you want to discover that tech...

## Useful Cucumber tips

### Multiple feature books

This sample supports adding another feature book on top of the first one, but you might need to do some refactoring of the `StepDefinitions` class if you want to recycle step definitions between books and use more than one step definition class.

### Conjunctions

Do not be afraid to split the `Given`, `When` and `Then` parts of your test on multiple lines ! This will actually be faster to write than putting all of them in a single statement. And even if that means multiple step definitions, you can do one of these things to shorten definition time or recycle step definitions :

- call the same step multiple times with different parameters each time within a scenario
- use the same step definition over multiple scenarios and/or books
- add other ways to call the same step definition

Conjunctions can also be used to write more precise `Then` clauses in your tests, like the `Or` one to handle crashes and/or false assertions.


[cucumber]: https://cucumber.io/docs/cucumber/
